#!/usr/bin/make -f
#
# Pieter Palmers <pieter.palmers@ffado.org>
#
# Robert Jordens <jordens@debian.org>
#
# This software may be used and distributed according to the terms
# of the GNU General Public License, incorporated herein by reference.
#

include /usr/share/dpkg/architecture.mk
include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/rules/utils.mk

DEB_CLEAN_EXCLUDE=debian/tmp

DEB_PYTHON2_MODULE_PACKAGES := ffado-tools ffado-mixer-qt4

### Add a soname ###

DEBIAN_FFADO_COMPATIBLE_VERSION = 2.0

CFLAGS += "-fPIC"

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  NJOBS += -j$(NUMJOBS)
endif

DEB_SCONS_EXTRA_FLAGS := \
	PREFIX=/usr \
	LIBDIR=/usr/lib/$(DEB_HOST_MULTIARCH) \
	SHAREDIR=/usr/share/libffado2/ \
	PYPKGDIR=/usr/share/ffado-mixer-qt4/ \
	WILL_DEAL_WITH_XDG_MYSELF=1 \
	DETECT_USERSPACE_ENV=0 \
	$(NJOBS)


DEB_SCONS_NOOPT_FLAGS := ENABLE_OPTIMIZATION=no

DEB_DH_MAKESHLIBS_ARGS := --version-info="libffado2 (>=${DEBIAN_FFADO_COMPATIBLE_VERSION})"

DEB_SCONS_ENVVARS :=
DEB_SCONS_INVOKE = $(DEB_SCONS_ENVVARS) scons 

DEB_SCONS_ARGS_COMMON = 'COMPILE_FLAGS=$(CFLAGS)' $(DEB_SCONS_EXTRA_FLAGS) \
				$(DEB_SCONS_NOOPT_FLAGS)

DEB_SCONS_ARGS_NODBG = $(DEB_SCONS_ARGS_COMMON) DESTDIR=$(DEB_DESTDIR) DEBUG=0
DEB_SCONS_ARGS_DBG = $(DEB_SCONS_ARGS_COMMON) DESTDIR=$(DEB_DESTDIR)/dbg DEBUG=1

#common-build-arch:: debian/stamp-scons-build
### Mangle libffado filename ###
debian/stamp-scons-build:
	-mkdir -p $(DEB_DESTDIR)
	$(DEB_SCONS_INVOKE) $(DEB_SCONS_ARGS_NODBG)
	$(DEB_SCONS_INVOKE) $(DEB_SCONS_ARGS_NODBG) install
	$(DEB_SCONS_INVOKE) $(DEB_SCONS_ARGS_DBG)
	$(DEB_SCONS_INVOKE) $(DEB_SCONS_ARGS_DBG) install
	touch debian/stamp-scons-build

# this is bad but the only easy way to have ardour.rc generated from
# ardour.rc.in
common-install-indep:: debian/stamp-scons-build
common-install-arch:: debian/stamp-scons-build

clean:: scons-clean
scons-clean::
	$(MAKE) -f debian/rules reverse-config
	-mkdir -p $(DEB_DESTDIR)
	-$(DEB_SCONS_INVOKE) --clean
	-rm -rf $(DEB_DESTDIR) debian/stamp-scons-build
	-rm -rf config.log scache.conf .sconf_temp .sconsign.dblite cache
	-rm -rf admin/*.pyc

binary-install/ffado-mixer-qt4::
	dh_python2 -pffado-mixer-qt4

binary-install/ffado-tools::
	sed -i 's/@@MULTIARCH@@/$(DEB_HOST_MULTIARCH)/' debian/ffado-tools/usr/bin/ffado-debug
	find debian/ffado-tools -type f -name "static_info.txt" -delete
	dh_python2 -pffado-tools
